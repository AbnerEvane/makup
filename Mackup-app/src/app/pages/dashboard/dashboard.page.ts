/* eslint-disable max-len */
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

   pros: any;
   services: boolean;
   extention: boolean;
   product: boolean;
  constructor() { }
  changeViewServices(){
    this.services = true;
    this.extention =false;
    this.product = false;
  }
  changeViewExtention(){
      this.services = false;
      this.extention =true;
      this.product = false;
  }
  changeViewProduct(){
    this.services = false;
    this.extention =false;
    this.product = true;
}
  ngOnInit() {
    this.retrievePros();
    this.changeViewServices();
  }
  retrievePros(){
    this.pros =[
      {
        name: 'Laurie Brille',
        profilImage: 'assets/Model1.png',
        date: '10/03/1994',
        statut: 'independante',
        contact: '048592499',
        discription: 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de limprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.',

      },{
        name: 'Linda YA',
        profilImage: 'assets/Model 3.png',
        date: '10/03/1994',
        statut: 'independante',
        contact: '048592499',
        discription: 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.',

      },{
        name: 'Gey Nze',
        profilImage: 'assets/Model1.png',
        date: '10/03/1994',
        statut: 'independante',
        contact: '048592499',
        discription: 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de',

      },{
        name: 'Hanna Brille',
        profilImage: 'assets/Model 4.png',
        date: '10/03/1994',
        statut: 'independante',
        contact: '048592499',
        discription: 'zhfizhfizrfzirfhzhfizeizhfzrihfzfhzrhzfzbzfzbfxzb',

      },{
        name: 'Laurie Brille',
        profilImage: 'assets/Model1.png',
        date: '10/03/1994',
        statut: 'independante',
        contact: '048592499',
        discription: 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de limprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.',

      },{
        name: 'Linda YA',
        profilImage: 'assets/Model 3.png',
        date: '10/03/1994',
        statut: 'independante',
        contact: '048592499',
        discription: 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.',

      },{
        name: 'Gey Nze',
        profilImage: 'assets/Model1.png',
        date: '10/03/1994',
        statut: 'independante',
        contact: '048592499',
        discription: 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de',

      },{
        name: 'Hanna Brille',
        profilImage: 'assets/Model 4.png',
        date: '10/03/1994',
        statut: 'independante',
        contact: '048592499',
        discription: 'zhfizhfizrfzirfhzhfizeizhfzrihfzfhzrhzfzbzfzbfxzb',

      },
    ];
  }


}
