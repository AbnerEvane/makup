import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {

  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  public url: string = '';
  constructor(private router: Router) { }

  ngOnInit() {
    this.url = this.router.url;
  }

  dashboardPage() {
    this.router.navigate(['dashboard']);
  }

  sellPage() {
    this.router.navigate(['product']);
  }

  msgPage() {
    this.router.navigate(['profil']);
  }
}
