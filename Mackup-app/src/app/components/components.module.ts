import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import { InfoBarComponent } from './info-bar/info-bar.component';


@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    InfoBarComponent,
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    InfoBarComponent,
  ],
  imports: [
    CommonModule,
    IonicModule.forRoot()
  ]
})
export class ComponentsModule { }
